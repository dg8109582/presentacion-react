import diego from '../assets/diego.jpg'

function ComponenteDos() {
  return (
    <>
        <div class="contenido">
            <div class="item">
            <img className='dgimg' src={diego} alt="" />
                    <div class="datos">
                        <h1>Nombre: Gonzalez Salazar Diego Antonio</h1>
                        <h1>NC: 19680158</h1>
                        <h1>Materia: Serv. Web en Disp. Mov</h1>
                        <h1>Carrera: ISC / Semestre: 8° / Grupo: 3</h1>
                        <h1>Docente: Guillermo Urzúa Sánchez</h1>
                    </div>
            </div>
        </div>
    </>
  )
}

export default ComponenteDos