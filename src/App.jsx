import { useState } from 'react'
import ComponenteDos from './Components/ComponenteDos'
import ComponenteTres from './Components/ComponenteTres'
import ComponenteUno from './Components/ComponenteUno'

function App() {
  const [count, setCount] = useState(0)

  return (
    <div >
      
      <ComponenteUno/>
      <ComponenteDos/>
      <ComponenteTres/>
     
    </div>
  )
}

export default App
